This collections of functions and tools are used to download and evaluated data related to the Infrared cameras, including thermocouples. 
To install this libary, run the setup

 python setup.py install
 
 after installation you can call:
 
 
 import downloadversionIRdata as IR
 
 IR.get_started()