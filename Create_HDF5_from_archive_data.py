# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 13:28:31 2017

@author: holn

save V2 into new files
"""
if __name__=='__main__':
    import h5py as h5
    import downloadversionIRdata as IR_DL
    import numpy as np
    localpath=""
    time_s=1.5

    programlist = ["20171010.025"]
    ports=[50]
    
    for program in programlist:
        for port in ports:
            try:
                exist,dimof,data1,valid=IR_DL.get_temp_from_raw_by_program(portnr=port,program=program,time_window=time_s,threads=4,verbose=5)
                if exist:
                    print("data found")
                    stream="images"
                    data1=np.asarray(data1)
                    data1=data1.swapaxes(0,2)
                    data1=data1.swapaxes(0,1)
                    dtype = str(data1.dtype)
                    tmpfile = localpath+"AEF"+str(port)+"_"+program+"_temp_V2"          
                    tmpfile += ".h5"
                    with h5.File(tmpfile, 'w') as f:    
                        f.create_dataset('timestamps', data=dimof, dtype='uint64' )#,compression="gzip")
                        dset=f.create_dataset(stream, shape=np.shape(data1),dtype=dtype , chunks = (np.shape(data1)[0],np.shape(data1)[1],1))
                        data1=np.ascontiguousarray(data1)
                        dset.write_direct(data1)            
                del data1
            except Exception as E:
                print(E)